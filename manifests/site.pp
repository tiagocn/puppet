node "default" {
	include config
}

node "docker.dexter.com.br" {
     include config
	include web
     include docker		
}
